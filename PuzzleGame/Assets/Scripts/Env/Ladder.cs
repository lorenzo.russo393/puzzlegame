using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ladder : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(!collision.GetComponent<PlayerMovement>()) return;

        PlayerMovement pm = collision.GetComponent<PlayerMovement>();
        pm.can_climbLadder = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (!collision.GetComponent<PlayerMovement>()) return;

        PlayerMovement pm = collision.GetComponent<PlayerMovement>();
        pm.can_climbLadder = false;
    }
}
