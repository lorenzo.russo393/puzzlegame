using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ground_Tile : MonoBehaviour
{
    [SerializeField] GameObject player;

    private void Start()
    {
        //Physics2D.IgnoreLayerCollision(transform.GetComponent<LayerMask>(), player.GetComponent<LayerMask>(), true);


        GenerateLedgePosition();
        //InvokeRepeating(nameof(GenerateLedgePosition), 0, 2);
    }

    private void GenerateLedgePosition()
    {
        GameObject right_ledge = new GameObject();
        right_ledge.name = "RightLedge";
        right_ledge.AddComponent<BoxCollider2D>().size = new Vector2(1, 1f);
        right_ledge.GetComponent<BoxCollider2D>().isTrigger = true;
        right_ledge.layer = LayerMask.NameToLayer("Ledge");

        right_ledge.transform.parent = transform;

        right_ledge.transform.localPosition = new Vector3(0.45f, 0.85f, 0);


        GameObject left_ledge = new GameObject();
        left_ledge.name = "LeftLedge";
        left_ledge.AddComponent<BoxCollider2D>().size = new Vector2(1f, 1f);
        left_ledge.GetComponent<BoxCollider2D>().isTrigger = true;
        left_ledge.layer = LayerMask.NameToLayer("Ledge");

        left_ledge.transform.parent = transform;

        left_ledge.transform.localPosition = new Vector3(-0.45f, 0.85f, 0);

        //ledge.transform.position = Vector2.zero;

    }
}
