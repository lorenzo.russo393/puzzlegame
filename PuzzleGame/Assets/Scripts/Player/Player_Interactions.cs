using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Player_Interactions : MonoBehaviour
{
    [Header("Interactables refs")]
    private TriggerDialogue trigger_dialogue;

    [Header("Interaction values")]
    [SerializeField] float npc_interaction_lineTrace = 2f;
    [SerializeField] float interaction_radius = 2f;

    [Header("Masks")]
    [SerializeField] LayerMask npc_layer;
    [SerializeField] LayerMask interactable_layer;

    [Header("Key Sprite stuff")]
    [SerializeField] SpriteRenderer KeySprite;

    private bool can_interact = false;
    private bool interaction_started = false;
    private IInteractable interactable;

    private void Update()
    {
        /*if (hit.collider == null)
        {
            can_InteractObject = false;
            KeySprite.gameObject.SetActive(false);

            return;
        }
        */


        

       /* if (interactable != null)
        {
            can_InteractObject = true;
            KeySprite.sprite = interactionKey_sprite;
            KeySprite.gameObject.SetActive(true);
        }
       */
    }

    public void InteractToNPC(InputAction.CallbackContext context)
    {
        if (!Physics2D.Raycast(transform.parent.position, PlayerMovement.direction.normalized, npc_interaction_lineTrace, npc_layer)) return;

        RaycastHit2D hit = Physics2D.Raycast(transform.position, PlayerMovement.direction.normalized, npc_interaction_lineTrace, npc_layer);

        //if (hit.collider == null) return;

        trigger_dialogue = hit.transform.GetComponent<TriggerDialogue>();

        if (trigger_dialogue != null && PlayerMovement.can_move)
        {
            trigger_dialogue.Trigger();
            PlayerMovement.can_move = false;
        }
    }

    public void InteractToInteractable(InputAction.CallbackContext context)
    {
        //if (!Physics2D.OverlapCircle(transform.position, interaction_radius, interactable_layer)) return;

        if (!can_interact || interactable == null) return;

        if (!interaction_started)
        {
            if(interactable.Type != InteractableType.NPC  && interactable.Type != InteractableType.Collectible)
                interaction_started = true;

            print(interactable);
            interactable.Interact(transform.parent.GetComponent<PlayerMovement>(), interaction_started);

            return;
        }
        else
        {
            interaction_started = false;
            interactable.StopInteract(interaction_started);

            return;
        }

        //Collider2D coll = Physics2D.OverlapBox(transform.position, transform.GetComponent<BoxCollider2D>().size, interactable_layer);
        //var interactable = coll.transform.GetComponent<IInteractable>();
    }

   

    private void OnTriggerEnter2D(Collider2D collision)
    {
        interactable = collision.gameObject.GetComponent<IInteractable>();

        if (interactable == null) return;   
        
        can_interact = true;
        //KeySprite.gameObject.SetActive(true);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        interactable = null;

        can_interact = false;
        KeySprite.gameObject.SetActive(false);
    }
}