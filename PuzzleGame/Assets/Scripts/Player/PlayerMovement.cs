using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody2D rb;

    #region AnimationEvents

    public static Action<bool> OnPlayerMove;
    public static Action OnPlayerIdle;
    public static Action OnPlayerJump;

    #endregion

    [Header("Movement values")]
    [SerializeField] float move_speed = 2;
    [SerializeField] float jump_force = 4;

    [Header("Masks")]
    [SerializeField] LayerMask ledge_layer;
    [SerializeField] LayerMask ground_layer;
    [SerializeField] LayerMask ladder_layer;
    [SerializeField] LayerMask startLadder_layer;
    [SerializeField] LayerMask endLadder_layer;

    [Header("Transforms")]
    [SerializeField] Transform interact_check;
    [SerializeField] Transform wall_check;
    [SerializeField] Transform ladder_check;
    [SerializeField] Transform ground_check;

    [Header("Sprites")]
    [SerializeField] SpriteRenderer player_spriteRenderer;

    [Header("Slope")]
    [SerializeField] float slope_checkDistance;
    private float slopeDownAngle;
    private float slopeSideAngle;
    private Vector2 slopeNormalPerpendicular;
    private bool isSlope;
    private float slopeDownAngleOld;
    [SerializeField] private PhysicsMaterial2D noFriction;
    [SerializeField] private PhysicsMaterial2D fullFriction;

    [SerializeField] private Vector2 horizontal_movement;
    [SerializeField] private Vector2 vertical_movement;
    public static Vector3 direction;
    public static bool can_move = true;
    public static bool can_rotate = true;
    public bool can_climbLadder = false;
    private Vector2 colliderSize;
    private Vector2 velocity = Vector2.zero;
    public float smoothTime = 0.3f;

    [SerializeField] Collider2D c;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        colliderSize = GetComponent<CapsuleCollider2D>().size;
        //Physics2D.gravity = new Vector2(0, -9.81f);
    }

    private void FixedUpdate()
    {
        SlopeCheck();
    }

    /* private void Update()
     {
         if (!Physics2D.OverlapCircle(ladder_check.position + new Vector3(0, 0.1f, 0), 0.1f, ladder_layer) ||
             !Physics2D.OverlapCircle(ladder_check.position + new Vector3(0, -0.2f, 0), 0.1f, ladder_layer))        
         {
             can_move = true;
             can_climbLadder = false;
             transform.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
             print("rrrrr");
             return;
         }
         else
         {
             can_climbLadder = true;
         }
     }
    */
    private void OnEnable()
    {
        DialogueSystem.OnDialogueFinished += CanMove;
        MovableObject.OnMovableObject += IsMovingObject;
    }

    private void OnDisable()
    {
        DialogueSystem.OnDialogueFinished -= CanMove;
        MovableObject.OnMovableObject -= IsMovingObject;
    }

    public void Move(Vector2 move)
    {
        if (!IsGrounded()) 
        {
            rb.sharedMaterial = null;
            OnPlayerMove?.Invoke(false);
            return;
        }

        if (move.x != 0)
        {
            direction = move;
            can_move = true;
            OnPlayerMove?.Invoke(true);
        }
        else
        {
            can_move = false;
            OnPlayerMove?.Invoke(false);
        }

        /*if (!can_move)
        {
            OnPlayerMove?.Invoke(false);
            return;
        */
        
        
        horizontal_movement = move;

        //rb.velocity = new Vector2(horizontal_movement.x * move_speed, rb.velocity.y);
        if (!isSlope && IsGrounded())
        {
            //Physics2D.gravity = Physics2D.gravity * Time.deltaTime;
            //currentVel = Vector3.SmoothDamp(currentVel, horizontal_movement * move_speed, ref smoothVelocity, 0.2f);
            //transform.position = Vector3.Lerp(transform.position, transform.position + horizontal_movement, Time.deltaTime * move_speed);
            //currentVel = Vector3.SmoothDamp(currentVel, new Vector3(horizontal_movement.x, rb.velocity.y, 0) * move_speed, ref smoothVelocity, 0.1f);
            //rb.velocity = horizontal_movement * move_speed ;
            //rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y);
            Vector2 targetVelocity = new Vector2(horizontal_movement.x * move_speed, 0);
            rb.velocity = Vector2.SmoothDamp(rb.velocity, targetVelocity, ref velocity, 0);
            //Vector2 pos = new Vector2(horizontal_movement.x, rb.velocity.y);
            //rb.MovePosition(rb.position + (pos * move_speed * Time.fixedDeltaTime));
            //rb.AddForce(rb.position + (pos * move_speed) * Time.deltaTime);
        }
        if(isSlope && IsGrounded()) 
        {
            rb.velocity = new Vector2(move_speed * slopeNormalPerpendicular.x * -horizontal_movement.x, move_speed * slopeNormalPerpendicular.y * -horizontal_movement.x);
            //rb.velocity.Set(move_speed * slopeNormalPerpendicular.x * -horizontal_movement.x, move_speed * slopeNormalPerpendicular.y * -horizontal_movement.x);
            //rb.MovePosition(rb.position + (-horizontal_movement * move_speed * Time.deltaTime * slopeNormalPerpendicular));
            //transform.position = Vector3.Lerp(transform.position, slopeNormalPerpendicular.y * transform.position + horizontal_movement, Time.deltaTime * move_speed);
        }
        // else

        if (move.x != 0 && can_rotate)
        {
            if (horizontal_movement.x < 0)
                transform.eulerAngles = new Vector2(0, 180);

            else
                transform.eulerAngles = new Vector2(0, 0);
        }
    }

    private bool IsGrounded()
    {
        bool is_grounded = true;

        if (Physics2D.OverlapCircle(ground_check.position, ground_check.GetComponent<CircleCollider2D>().radius, ground_layer))
            is_grounded = true;
        else
            is_grounded = false;

        return is_grounded;
    }

    public void NotMove() => OnPlayerMove?.Invoke(false);

    private void SlopeCheck()
    {
        Vector2 checkPosition = transform.position - new Vector3(0.0f, colliderSize.y / 3);

        SlopeCheckVertical(checkPosition);

       // SlopeCheckHorizontal(checkPosition);

        //RaycastHit2D horizontal_hit = Physics2D.Raycast(slope_check.position, Vector2.right, slope_checkDistance, ground_layer);

        
    }

    private void SlopeCheckVertical(Vector2 check)
    {
        RaycastHit2D hit = Physics2D.Raycast(check, Vector2.down, slope_checkDistance, ground_layer);

        if (hit)
        {
            slopeNormalPerpendicular = Vector2.Perpendicular(hit.normal).normalized;
            slopeDownAngle = Vector2.Angle(hit.normal, Vector2.up);

            if(slopeDownAngle != slopeDownAngleOld)
            {
                isSlope = true;
            }

            slopeDownAngleOld = slopeDownAngle;

            Debug.DrawRay(hit.point, hit.normal, Color.yellow);
            Debug.DrawRay(hit.point, slopeNormalPerpendicular, Color.red);

            if (IsGrounded())
                rb.sharedMaterial = fullFriction;

            if (isSlope && horizontal_movement.x != 1 && horizontal_movement.x != -1)
            {
                rb.sharedMaterial = fullFriction;
            }
            else
            {
                rb.sharedMaterial = noFriction;
            }
        }        
    }

    private void SlopeCheckHorizontal(Vector2 check)
    {
        RaycastHit2D hitFront = Physics2D.Raycast(check, transform.right, slope_checkDistance, ground_layer);
        RaycastHit2D hitBack = Physics2D.Raycast(check, -transform.right, slope_checkDistance, ground_layer); 

        if(hitFront )
        {
            isSlope = true;
            slopeSideAngle = Vector2.Angle(hitFront.normal, Vector2.up);
        }
        else if(hitBack)
        {
            isSlope = true;
            slopeSideAngle  = Vector2.Angle(hitBack.normal, Vector2.up);
        }     
        else
        {
            print("yea");
            isSlope = false;
            if(slopeSideAngle > 0)
            slopeSideAngle -= Time.deltaTime;
        }
    }

    public void Climb(InputAction.CallbackContext context)
    {
        //if (!Physics2D.Raycast(transform.position, direction.normalized, 1, wall_layer)) return;

        if (!Physics2D.OverlapBox(wall_check.position, new Vector2(0.13f, 3.3f), 1, ledge_layer)) return;

        if (!Physics2D.Raycast(transform.position, direction.normalized, 1f)) return;

        c = Physics2D.OverlapBox(wall_check.position, new Vector2(0.13f, 3.3f), 1, ledge_layer);



        //RaycastHit2D hit = Physics2D.Raycast(transform.position, c.transform.position, 2, wall_layer);

        /*if (hit)
        {
            float distance = hit.distance;

            print(distance);

            pos = new Vector2(transform.position.x + distance, transform.position.y + hit.point.y);

        }
        */
            
        

        StartCoroutine(ClimbOverWall(c.gameObject.transform));

        can_move = false;
    }

    IEnumerator ClimbOverWall(Transform point)
    {
        //yield return new WaitForSeconds(animator.GetCurrentAnimatorStateInfo(0).length);

        yield return new WaitForSeconds(0.5f);

        transform.position = new Vector2(point.transform.position.x, point.transform.position.y);

        can_move = true;
    }
    private void CanMove() => can_move = true;
    private void IsMovingObject(bool isMoving)
    {
        if (isMoving)
        {
            move_speed /= 2;
            can_rotate = false;
        }
        else
        {
            move_speed *= 2;
            can_rotate = true;  
        }
    }

    public void MoveUp(Vector2 move)
    {
        //if (!can_climbLadder && !Physics2D.OverlapCapsule(ladder_check.position, ladder_check.GetComponent<CapsuleCollider2D>().size, CapsuleDirection2D.Horizontal, 1, endLadder_layer))
        //  return;

        if (move.y > 0)
        {
            if (!Physics2D.OverlapCircle(ladder_check.position + new Vector3(0, 0.1f, 0), 0.1f, ladder_layer))
            {
                transform.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
                can_move = true;
                return;
            }
        }
        else if (move.y == -1)
        {
            if (!Physics2D.OverlapCircle(ladder_check.position + new Vector3(0, -0.2f, 0), 0.1f, ladder_layer))
            {
                transform.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
                can_move = true;
                return;
            }
        }

        vertical_movement = move;

        can_move = false;
        print("ff");
        
        transform.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
        rb.MovePosition(rb.position + (vertical_movement * move_speed * Time.deltaTime));
        //transform.position = Vector3.Lerp(transform.position, transform.position + vertical_movement, Time.deltaTime * move_speed);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        //Gizmos.DrawWireSphere(transform.position, interaction_radius);
        //Gizmos.DrawWireCube(transform.position, transform.GetComponent<BoxCollider2D>().size);

        Gizmos.color = Color.green;
        Gizmos.DrawLine(transform.position, transform.position + direction);
        Gizmos.DrawWireSphere(ladder_check.position + new Vector3(0, 0.1f, 0), 0.1f);
        Gizmos.DrawWireSphere(ladder_check.position + new Vector3(0, -0.2f, 0), 0.1f);
    }
}
