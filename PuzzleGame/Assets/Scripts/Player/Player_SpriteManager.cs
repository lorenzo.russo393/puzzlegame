using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_SpriteManager : MonoBehaviour
{
    //[SerializeField] private SpriteRenderer sr;

    //[SerializeField] Sprite[] idle_sprites;

    [SerializeField] Animator animator;

    private void Start()
    {
        //StartCoroutine(IdleAnimation());
    }

    private void OnEnable()
    {
        PlayerMovement.OnPlayerMove += MoveAnimation;
    }
    private void OnDisable()
    {
        PlayerMovement.OnPlayerMove -= MoveAnimation;
    }
    

    private void MoveAnimation(bool canMove)
    {
        animator.SetBool("CanMove", canMove);
    }

    /* public IEnumerator IdleAnimation()
     {
         for(int i = 0; i < idle_sprites.Length; i++)
         {
             sr.sprite = idle_sprites[i];

             if(i == idle_sprites.Length - 1)
             {
                 i = 0;
             }

             yield return new WaitForSeconds(0.2f);
         }
     }
    */
}
