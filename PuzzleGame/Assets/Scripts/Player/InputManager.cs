using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputManager : MonoBehaviour
{
    [SerializeField] private PlayerMovement pm;
    [SerializeField] private Player_Interactions pi;
    [SerializeField] private DialogueSystem ds;
    [SerializeField] private InventoryUI iu;

    Controls player_controls;
    Controls.MovementActions movement;
    Controls.InteractionActions interaction;
    Controls.DialogueActions dialogue;
    Controls.InventoryActions inventory;

    Vector2 horizontal;
    Vector2 vertical;

    private void Awake()
    {
        //pa = GetComponent<PlayerActions>();
        //pi = GetComponent<Playe_Interactions>();
        player_controls = new Controls();

        movement = player_controls.Movement;
        interaction = player_controls.Interaction;
        dialogue = player_controls.Dialogue;
        inventory = player_controls.Inventory;

        movement.Move.performed += ctx => horizontal = ctx.ReadValue<Vector2>();
        movement.MoveUp.performed += ctx => vertical = ctx.ReadValue<Vector2>();
        movement.Jump.performed += pm.Climb;
        interaction.Interact.performed += pi.InteractToNPC;
        interaction.Interact.performed += pi.InteractToInteractable;
        dialogue.NextDilaogue.performed += ds.GetInput;

        inventory.OpenInventory.performed += iu.OpenInventory;
    }

    private void OnEnable()
    {
        player_controls.Enable();
    }

    private void OnDisable()
    {
        player_controls.Disable();
    }

    private void FixedUpdate()
    {

        //if (movement.Move.IsPressed())
            pm.Move(horizontal);


        if (movement.MoveUp.IsPressed())
            pm.MoveUp(vertical);
    }
}
