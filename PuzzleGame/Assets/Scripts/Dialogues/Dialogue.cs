using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Dialogue
{
    public string name;
    public Sprite sprite;
    public string keyWordOrPhrase;
    [Space]
    [TextArea(2, 2)]
    public string[] texts;
}
