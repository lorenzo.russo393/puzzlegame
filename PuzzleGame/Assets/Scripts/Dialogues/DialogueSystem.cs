using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;

public class DialogueSystem : MonoBehaviour
{
    public static Action OnDialogueFinished;

    IEnumerator write_coroutine;
    public Queue<string> sentences;
    private Dialogue[] current_dialogue;

    public int currentPerson = 0;
    public int currentText = 0;

    private bool dialogue_started = false;

    [Header("Dialogues panels")]
    [SerializeField] GameObject background_panel;
    [SerializeField] GameObject background_dialogue;

    [Header("Texts")]
    [SerializeField] TextMeshProUGUI name_text;
    [SerializeField] TextMeshProUGUI text;

    private void Start()
    {
        sentences = new Queue<string>();
    }

    public void GetInput(InputAction.CallbackContext context)
    {
        if(dialogue_started)
            GoNext(current_dialogue);
    }

    public void StartDialogue(Dialogue[] dialogue)
    {
        background_dialogue.SetActive(true);
        background_panel.SetActive(true);

        dialogue_started = true;

        currentPerson = 0;
        currentText = 0;
        GoNext(dialogue);
    }

    public void GoNext(Dialogue[] dialogue)
    {
        text.text = "";
        name_text.text = "";

        current_dialogue = dialogue;
        sentences.Clear();

        if (current_dialogue == null)
            return;

        if (currentText >= current_dialogue[currentPerson].texts.Length)
        {
            currentPerson++;
            currentText = 0;
        }

        if(currentPerson < current_dialogue.Length)
        {
            sentences.Enqueue(dialogue[currentPerson].texts[currentText]);
            NextDialogue(dialogue[currentPerson]);
            currentText++;
        }
        else
        {
            IsDialogueFinished();
        }
    }

    private void NextDialogue(Dialogue dialogue)
    {
        string sentence = "";
        name_text.text = dialogue.name;
        sentence = sentences.Dequeue();

        if (write_coroutine != null)
        {
            StopCoroutine(write_coroutine);
        }

        write_coroutine = WriteText(sentence);
        StartCoroutine(write_coroutine);
    }

    private IEnumerator WriteText(string sentence)
    {
        foreach(char c in sentence)
        {
            text.text += c.ToString();
            yield return new WaitForSeconds(0.05f);
            //yield return null;
        }
    }

    private void IsDialogueFinished()
    {
        if(currentPerson >= current_dialogue.Length)
        {
            OnDialogueFinished?.Invoke();
            dialogue_started = false;
            currentPerson = 0;
            currentText = 0;

            StopCoroutine(write_coroutine);

            text.text = "";
            name_text.text = "";

            background_panel.SetActive(false);
            background_dialogue.SetActive(false);
        }
    }
}
