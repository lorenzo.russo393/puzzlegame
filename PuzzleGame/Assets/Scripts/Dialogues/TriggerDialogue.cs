using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDialogue : MonoBehaviour
{
    [SerializeField]
    Dialogue[] dialogue;

    public void Trigger()
    {
        FindObjectOfType<DialogueSystem>().StartDialogue(dialogue);

        /*if (FindObjectOfType<DialogueSystem>().currentPerson < dialogue.Length)
        {
            FindObjectOfType<DialogueSystem>().StartDialogue(dialogue[FindObjectOfType<DialogueSystem>().currentPerson]);
        }
        */
    }
}