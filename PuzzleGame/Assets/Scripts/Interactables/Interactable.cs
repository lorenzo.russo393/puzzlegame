using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum InteractableType { NPC, Object, Collectible }
//public enum CollectibleType { Text, Sprite }

public interface IInteractable
{
    public InteractableType Type { get; }
    //public CollectibleType CollType { get;}

    //public UnityEvent OnInteract { get; protected set; }+

    public abstract void Interact(PlayerMovement pa, bool isInteracting);
    public abstract void StopInteract(bool isInteracting);
}

public abstract class Interactable : MonoBehaviour, IInteractable
{
    public InteractableType type;
   // public CollectibleType collType;
    //public CollectibleType CollType => collType;
    public InteractableType Type => type;

    //public CollectibleType CollType => throw new System.NotImplementedException();

    public abstract void Interact(PlayerMovement pa, bool isInteracting);
    public abstract void StopInteract(bool isInteracting);
}
