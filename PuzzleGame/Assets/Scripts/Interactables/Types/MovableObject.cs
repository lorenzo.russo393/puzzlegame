using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovableObject : Interactable
{
    public static Action<bool> OnMovableObject;

    public override void Interact(PlayerMovement pm, bool isInteracting)
    {
        //transform.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        transform.parent = pm.transform;
        OnMovableObject?.Invoke(isInteracting);
    }

    public override void StopInteract(bool isInteracting)
    {
        //transform.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
        transform.parent = null;
        OnMovableObject?.Invoke(isInteracting);
    }
}
