using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : Interactable
{
    [Header("Dialogue")]
    [SerializeField] Dialogue[] dialogue;

    public override void Interact(PlayerMovement pm, bool isInteracting)
    {
        FindObjectOfType<DialogueSystem>().StartDialogue(dialogue);
    }

    public override void StopInteract(bool isInteracting)
    {
        
    }
}
