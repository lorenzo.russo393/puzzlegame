using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor;
using UnityEditor.TerrainTools;
using UnityEngine;

public class Collectible : Interactable
{
    public static event Action<Collectible> OnCollectiblePickedUp;
    public int page_index = 0;

    public enum CollectibleType { Text, Sprite}
    public CollectibleType collectibleType;
    public Sprite spriteOnPage;
    public String textOnPage;


    public override void Interact(PlayerMovement pm, bool isInteracting)
    {
        OnCollectiblePickedUp?.Invoke(transform.GetComponent<Collectible>());
        Destroy(gameObject);
    }

    public override void StopInteract(bool isInteracting)
    {
           
    }
}

[CustomEditor(typeof(Collectible))]
public class CollectibleEditor : Editor
{
    private SerializedProperty collectible_type;
    private SerializedProperty image;
    private SerializedProperty text;
    private SerializedProperty index;

    private void OnEnable()
    {
        collectible_type = serializedObject.FindProperty("collectibleType");
        image = serializedObject.FindProperty("spriteOnPage");
        text = serializedObject.FindProperty("textOnPage");
        index = serializedObject.FindProperty("page_index");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(index);
        EditorGUILayout.Space(10);
        EditorGUILayout.PropertyField(collectible_type);

        Collectible.CollectibleType collType = (Collectible.CollectibleType)collectible_type.enumValueIndex;

        switch(collType)
        {
            case Collectible.CollectibleType.Text:
                EditorGUILayout.PropertyField(text);
                break;
            case Collectible.CollectibleType.Sprite:
                EditorGUILayout.PropertyField(image);
                break;


        }

        serializedObject.ApplyModifiedProperties();


    }
}
