using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class InventoryUI : MonoBehaviour
{
    [SerializeField] GameObject all_inventory;
    [SerializeField] GameObject all_collectibles;
    [SerializeField] GameObject all_characters;
    [SerializeField] List<TextMeshProUGUI> collectibleText_list = new List<TextMeshProUGUI>();
    [SerializeField] List<UnityEngine.UI.Image> collectibleSprite_list = new List<UnityEngine.UI.Image>();
    [SerializeField] List<GameObject> character_list = new List<GameObject>();
    [Space]
    int currentList = 0;
        
    private void Start()
    {
        FillImageList();
        FillTextList();

        /*int totalCollectibles = all_collectibles.transform.childCount;
        for (int i = 0; i < totalCollectibles; i++)
        {
            collectible_list.Add(all_collectibles.transform.GetChild(i).gameObject);
        }

        int totalCharacters = all_characters.transform.childCount;
        for (int i = 0; i < totalCharacters; i++)
        {
            character_list.Add(all_characters.transform.GetChild(i).gameObject);
        }
        */
    }

    private void OnEnable()
    {
        InventorySystem.OnCollectibleAdded += AddCollectibleUI;
    }

    private void OnDisable()
    {
        InventorySystem.OnCollectibleAdded += AddCollectibleUI;
    }

    private void FillTextList()
    {
        int totalCollectibles = all_collectibles.transform.childCount;
        for (int i = 0; i < totalCollectibles; i++)
        {
            if(all_collectibles.transform.GetChild(i).GetComponent<TextMeshProUGUI>())
                collectibleText_list.Add(all_collectibles.transform.GetChild(i).GetComponent<TextMeshProUGUI>());
        }
    }
    private void FillImageList()
    {
        int totalCollectibles = all_collectibles.transform.childCount;
        for (int i = 0; i < totalCollectibles; i++)
        {
            if (all_collectibles.transform.GetChild(i).GetComponent<UnityEngine.UI.Image>())
                collectibleSprite_list.Add(all_collectibles.transform.GetChild(i).GetComponent<UnityEngine.UI.Image>());
        }
    }

    public void OpenInventory(InputAction.CallbackContext context)
    {
        if(!InventorySystem.isInventoryOpen)
        {
            all_inventory.SetActive(true);
            InventorySystem.isInventoryOpen = true;
            return;
        }
        else
        {
            all_inventory.SetActive(false);
            InventorySystem.isInventoryOpen = false;
            return;
        }            
    }

    private void AddCollectibleUI(int index, Collectible collectible)
    {
        if(collectible.collectibleType == Collectible.CollectibleType.Sprite)
        {
            collectibleSprite_list[collectible.page_index].gameObject.SetActive(true);
            collectibleSprite_list[collectible.page_index].sprite = collectible.spriteOnPage;
        }
        else
        {
            collectibleText_list[collectible.page_index].gameObject.SetActive(true);
            collectibleText_list[collectible.page_index].text = collectible.textOnPage;
        }
    }
}
