using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventorySystem : MonoBehaviour
{
    public static event Action<int, Collectible> OnCollectibleAdded;

    public List<Collectible> collectiblesOwned = new List<Collectible>();
    public static bool isInventoryOpen = false;
    public int currentIndex = 0;

    private void Start()
    {
        
    }

    private void OnEnable()
    {
        Collectible.OnCollectiblePickedUp += AddCollectible;
    }

    private void OnDisable()
    {
        Collectible.OnCollectiblePickedUp -= AddCollectible;
    }

    public void AddCollectible(Collectible collectible)
    {
        collectiblesOwned.Add(collectible);
        OnCollectibleAdded?.Invoke(collectible.page_index, collectible);
    }
}
